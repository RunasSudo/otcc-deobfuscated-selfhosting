#!/bin/bash
set -e

gcc -O2 -m32 otccelf.c -o otccelf
chmod +x otccelf

./otccelf otccelf.c otccelf.otcc
chmod +x otccelf.otcc

./otccelf.otcc otccelf.c otccelf.otcc2
chmod +x otccelf.otcc2

./otccelf.otcc2 otccex.c otccex
chmod +x otccex

./otccex
